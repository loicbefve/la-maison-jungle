import {plantList} from "../datas/plantList";
import '../styles/ShoppingList.css'
import PlantItem from "./PlantItem";
import {Dispatch, SetStateAction, useState} from "react";
import {CartElem} from "./App";
import Categories from "./Categories";

interface IProps {
    cart: CartElem[],
    updateCart: Dispatch<SetStateAction<CartElem[]>>
}


function ShoppingList({cart, updateCart}: IProps) {

    const [category, updateCategory] = useState('')


    function addToCart(
        plant_id: string,
        plant_name: string,
        plant_price: number
    ) {
        const elem_to_update = cart.find(cart_elem => cart_elem.id === plant_id)
        if (elem_to_update) {
            const other_items = cart.filter(item => item !== elem_to_update)
            const updatedElem = {
                id: elem_to_update.id,
                name: elem_to_update.name,
                unitPrice: elem_to_update.unitPrice,
                quantity: elem_to_update.quantity + 1
            }
            updateCart([...other_items, updatedElem])
        } else {
            const newElem = {
                id: plant_id,
                name: plant_name,
                unitPrice: plant_price,
                quantity: 1
            }
            updateCart([...cart, newElem])
        }
    }

    return (
        <div className='lmj-shopping-list'>
            <Categories category={category} updateCategory={updateCategory}/>
            <ul className='lmj-plant-list'>
                {
                    plantList
                        .filter(plant => plant.category === category || category === '')
                        .map((plant) =>
                            <div key={plant.id}>
                                <PlantItem name={plant.name}
                                           cover={plant.cover}
                                           light={plant.light}
                                           water={plant.water}
                                           price={plant.price}
                                />
                                <button onClick={() => addToCart(plant.id, plant.name, plant.price)}>Ajouter</button>
                            </div>
                        )
                }
            </ul>
        </div>
    )
}

export default ShoppingList