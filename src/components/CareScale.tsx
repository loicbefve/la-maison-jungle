import {PropsWithChildren} from "react";
import Sun from '../assets/sun.svg'
import Water from '../assets/water.svg'

export enum CareType {
    Water,
    Light
}

type CareScaleProps = {
    scaleValue: number,
    careType: CareType
}

function CareScale( props: PropsWithChildren<CareScaleProps> ) {

    const {scaleValue, careType} = props
    const range = [1,2,3]
    const quantityLabel = new Map()
    quantityLabel.set(1,'peu')
    quantityLabel.set(2,'modérément')
    quantityLabel.set(3,'beaucoup')

    const care_message: string = careType === CareType.Light ? `Cette plante requiert ${quantityLabel.get(scaleValue)} de lumière.` : `Cette plante requiert ${quantityLabel.get(scaleValue)} d'eau.`

    const scaleType = careType === CareType.Light ? <img src={Sun} alt='sun icon'/> : <img src={Water} alt='water icon'/>

    return (
        <div onClick={(_) => alert(care_message)}>
            {
                range.map((rangeElem) =>
                    scaleValue >= rangeElem && <span key={rangeElem.toString()}>{scaleType}</span>
                )
            }️
        </div>
    )
}

export default CareScale