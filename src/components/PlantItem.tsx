import CareScale, {CareType} from "./CareScale";
import '../styles/PlantItem.css'

interface IProps {
    name: string,
    cover: string,
    light: number,
    water: number,
    price: number
}
export default function PlantItem( {
    name,
    cover,
    light,
    water,
    price
} : IProps) {

    return (
        <li className='lmj-plant-item'>
            <span className='lmj-plant-item-price'>{price}€</span>
            <img className='lmj-plant-item-cover' src={cover} alt={`${name} cover`}/>
            {name}
            <div>
                <CareScale scaleValue={light} careType={CareType.Light}/>
                <CareScale scaleValue={water} careType={CareType.Water} />
            </div>
        </li>
    )

}