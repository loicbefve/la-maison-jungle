import '../styles/Banner.css'
import {PropsWithChildren} from "react";

function Banner(props:PropsWithChildren) {
    const children = props.children
    return <div className='lmj-banner'>{children}</div>
}

export default Banner