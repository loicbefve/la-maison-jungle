import {Dispatch, SetStateAction, useState} from 'react'
import '../styles/Cart.css'
import {CartElem} from "./App";


interface IProps{
    cart: CartElem[],
    updateCart:Dispatch<SetStateAction<CartElem[]>>
}
function Cart({cart, updateCart}: IProps) {

    const [isOpen, setIsOpen] = useState(true)

    const totalPrice = cart.reduce((acc, current) => acc + (current.unitPrice*current.quantity), 0)

    return isOpen ? (
        <div className='lmj-cart'>
            <button
                className='lmj-cart-toggle-button'
                onClick={() => setIsOpen(false)}
            >
                Fermer
            </button>
            <h2>Panier</h2>
            {cart.map(({id,name, unitPrice, quantity}) =>
                <div key={id}>{quantity} X {name} : {unitPrice*quantity}€</div>
            )}
            <h3>Total : {totalPrice}€</h3>
            <button onClick={() => updateCart([] as CartElem[])}>Vider le panier</button>
        </div>
    ) : (
        <div className='lmj-cart-closed'>
            <button
                className='lmj-cart-toggle-button'
                onClick={() => setIsOpen(true)}
            >
                Ouvrir le Panier
            </button>
        </div>
    )
}

export default Cart