import {plantList} from "../datas/plantList";
import {Dispatch, SetStateAction} from "react";
import '../styles/Categories.css'

interface IProps {
    category: string,
    updateCategory: Dispatch<SetStateAction<string>>
}

function Categories({category, updateCategory}: IProps) {

    const categories = plantList.reduce(
        (acc, plant) =>
            acc.includes(plant.category) ? acc : acc.concat(plant.category), [] as string[]
    )

    return (
        <div className='lmj-categories'>
            <select value={category}
                    onChange={e => updateCategory(e.target.value)}
                    className='lmj-categories-select'
            >
                <option value=''>---</option>
                {
                    categories.map((cat) => (
                        <option key={cat} value={cat}>
                            {cat}
                        </option>
                    ))
                }
            </select>
            <button onClick={() => updateCategory('')}>Réinitialiser</button>
        </div>

)
}

export default Categories