# La maison jungle (tutoriel) :

## Présentation :

Ce site web écrit en React est le résultat du tutoriel openclassroom :  https://openclassrooms.com/fr/courses/7008001-debutez-avec-react
La particularité est que j'ai décidé, contrairement au tutoriel d'utiliser le **Typescript** plutôt que le **JavaScript**.


Il s'agit d'un site de commerce simple dans lequel quelques articles (des plantes) sont présents et on est capable
d'ajouter ces articles dans un panier qui affiche alors le prix total.

Les concepts React vues sont : 
- Les composants fonctions
- Les props
- Les hooks useState et useEffect

## Résultat :

![result.png](result.png)

## Certificat de réussite :

![certificat.png](certificat.png)
